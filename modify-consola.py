import fontforge                                 #Load the module
import psMat
cnsl=fontforge.open("consola.ttf")               #Open a font

em = cnsl.em
em_half = em/2

def getwidth(g):
    (xmin, ymin, xmax, ymax) = g.boundingBox()
    rsb = g.right_side_bearing
    width = xmax + rsb
    return width

for i in cnsl:
    width = getwidth(cnsl[i])
    if width == 0:
        continue
    a = em_half/width
    A = psMat.scale(a, a)
    cnsl[i].transform(A, ("round",))
    cnsl[i].addExtrema()  # add extrema

# change font name
cnsl.fontname = "NewConsolas"
cnsl.fullname = "New Consolas for VS Code"
cnsl.familyname = "New Consolas"

# change the UniqueID in TTF name.
hugo = list(cnsl.sfnt_names)
for i in range(0, len(hugo)):
    if hugo[i][1] == "UniqueID":
        hugo[i] = (hugo[i][0], hugo[i][1], "New Consolas for VS Code")
cnsl.sfnt_names = tuple(hugo)

# cnsl.save("NewConsolas.sfd")
cnsl.generate("NewConsolas.ttf")
