# Fontforge 字体修改脚本

使用 Fontforge 修改某些字体以使 VS Code 中英文字体可以对齐.

修改效果请访问: <https://blog.lrtfm.com/2020/05/2020-05-07-vscode-zh-font/>.
