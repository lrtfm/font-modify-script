import fontforge                                 #Load the module
import psMat
cnsl=fontforge.open("msyh.ttc")               #Open a font

em = cnsl.em
em_half = em/2

def getwidth(g):
    (xmin, ymin, xmax, ymax) = g.boundingBox()
    rsb = g.right_side_bearing
    width = xmax + rsb
    return width

a = 0.9094138543516874   # 这个数是保证该字体和 New Consola 使用同样的缩放比.

for i in cnsl:
    width = getwidth(cnsl[i])
    if width == 0:
        continue
    A = psMat.scale(a, a)
    cnsl[i].transform(A, ("round",))
    B = psMat.translate(width*(1-a)/2, 0)
    cnsl[i].transform(B, ("round",))
    new_width = getwidth(cnsl[i])
    if new_width < width:
        cnsl[i].right_side_bearing = int(cnsl[i].right_side_bearing + width - new_width)
    cnsl[i].addExtrema()  # add extrema


# change font name
cnsl.fontname = "NewMicrosoftYaHei "
cnsl.fullname = "New Microsoft YaHei for VS Code"
cnsl.familyname = "New Microsoft YaHei"

# change the UniqueID in TTF name.
hugo = list(cnsl.sfnt_names)
for i in range(len(hugo)-1, -1, -1):
    if hugo[i][0] == "English (US)":
        if hugo[i][1] == "UniqueID":
            hugo[i] = (hugo[i][0], hugo[i][1], "New Microsoft YaHei for VS Code")
    elif hugo[i][0] == "Chinese (PRC)":
        if hugo[i][1] == "Family" or hugo[i][1] == "Fullname" or hugo[i][1] == "UniqueID" :
            hugo[i] = (hugo[i][0], hugo[i][1], "新微软雅黑")
    else:
        hugo.pop(i)

cnsl.sfnt_names = tuple(hugo)

# cnsl.save("NewMSYH.sfd")      # 保存字体文件
cnsl.generate("NewMSYH.ttf")  # 生成字体
